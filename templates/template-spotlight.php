<?php
/*Template name: Spotlight*/

get_header(); ?>

<div class="page-category page-spotlight">
    <div class="container-fluid px-0 page-header">
        <div class="row no-gutters">
            <?php
            // vars

            $cImage = get_field('c_image');
            $cColor = get_field('c_bg_color');
            $cTitle = get_field('c_title');
            $cArrow = get_field('c_arrow_color');
            ?>
            <div class="col-12 col-md-6 order-2 order-md-1">
                <div class="position-relative">
                    <?php if (!empty($cImage)) { ?>
                        <img src="<?php echo $cImage; ?>" alt="" class="page-header__image">
                    <?php } ?>
                </div>
            </div>
            <div class="col-12 col-md-6 position-relative mobile-height order-1 order-md-2"
                 style="<?php if (!empty($cColor)) echo 'background-color:' . $cColor . '' ?>">
                <?php
                if (!empty($cTitle['text'])) { ?>
                    <h1 class="section-title" style="color: <?php echo $cTitle['color']; ?>">
                        <?php echo $cTitle['text']; ?>
                    </h1>
                <?php } ?>
                <a href="#content" class="scroll-down">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.199" height="28.799" viewBox="0 0 25.199 28.799">
                        <g id="arrow-down" transform="translate(-5.401 -3.6)">
                            <path id="Path_17" data-name="Path 17"
                                  d="M30.073,18.527a1.8,1.8,0,0,1,0,2.545l-10.8,10.8a1.8,1.8,0,0,1-2.545,0l-10.8-10.8a1.8,1.8,0,1,1,2.545-2.545L16.2,26.255V5.4a1.8,1.8,0,0,1,3.6,0V26.255l7.727-7.727a1.8,1.8,0,0,1,2.545,0Z"
                                  fill="<?php echo $cArrow; ?>" fill-rule="evenodd"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="container py-5 mt-md-4" id="content">
        <div class="row justify-content-center text-center">
            <div class="col-12 col-md-10">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    the_content();
                    // End the loop.
                endwhile; ?>
                <hr class="my-5">
            </div>
        </div>
    </div>
    <div class="container-fluid px-4">
        <div class="row mb-3">
            <div class="col-12 col-md-3 mb-3">
                <h3 class="filter-text mb-0">ALL ARTICLES</h3>
            </div>
            <div class="col-12 col-md-6 mb-3 ml-auto">
                <div class="select-container float-md-right">
                    <label for="sort">SORTY BY:</label>
                    <select name="sort" id="sort" data-order="" data-orderby="" class="places-sort">
                        <option value="a-z" data-order="asc" data-orderby="name" class="places-sort__option">Alphabetical
                            A-Z
                        </option>
                        <option value="z-a" data-order="desc" data-orderby="name" class="places-sort__option ">Alphabetical
                            Z-A
                        </option>
                        <option value="date-asc" data-order="asc" data-orderby="date" class="places-sort__option ">By Date
                            Ascending
                        </option>
                        <option value="date-desc" data-order="desc" data-orderby="date" class="places-sort__option">By Date
                            Descending
                        </option>
                    </select>
                </div>
            </div>
        </div>
        <?php

        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'order' => 'asc',
            'orderby' => 'name',
            'posts_per_page' => -1,
        );

        // The Query
        $query = new WP_Query($args); ?>
        <div class="row places articles mb-5">
            <?php
            // The Loop
            if ($query->have_posts()) {
                while ($query->have_posts()) {
                    $query->the_post();
                    get_template_part('parts/part','content');
                }
            } else {
                ?>

                <div class="col-12">
                    <h3>We're sorry, there are no articles</h3>
                </div>
            <?php }
            // Restore original Post Data
            wp_reset_postdata();
            ?>
        </div>
    </div>
</div>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/filters.js"></script>
<?php get_footer(); ?>

