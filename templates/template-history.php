<?php
/*Template name: History*/

get_header();
?>
<div class="page-history">
    <div class="container-fluid px-0 page-header">
        <div class="row no-gutters">
            <?php
            // vars
            $hImage = get_field('h_image');
            $hColor = get_field('h_bg_color');
            $hTitle = get_field('h_title');
            ?>
            <div class="col-12 col-md-6 order-2 order-md-1">
                <div class="position-relative">
                    <?php if (!empty($hImage)) { ?>
                        <img src="<?php echo $hImage; ?>" alt="" class="page-header__image">
                    <?php } ?>
                </div>
            </div>
            <div class="col-12 col-md-6 position-relative mobile-height order-1 order-md-1"
                 style="<?php if (!empty($hColor)) echo 'background-color:' . $hColor . '' ?>">

                <?php
                if (!empty($hTitle['text'])) { ?>
                    <h1 class="section-title" style="color: <?php echo $hTitle['color']; ?>">
                        <?php echo $hTitle['text']; ?>
                    </h1>
                <?php } ?>
                <a href="#content" class="scroll-down">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.199" height="28.799" viewBox="0 0 25.199 28.799">
                        <g id="arrow-down" transform="translate(-5.401 -3.6)">
                            <path id="Path_17" data-name="Path 17"
                                  d="M30.073,18.527a1.8,1.8,0,0,1,0,2.545l-10.8,10.8a1.8,1.8,0,0,1-2.545,0l-10.8-10.8a1.8,1.8,0,1,1,2.545-2.545L16.2,26.255V5.4a1.8,1.8,0,0,1,3.6,0V26.255l7.727-7.727a1.8,1.8,0,0,1,2.545,0Z"
                                  fill="#fff" fill-rule="evenodd"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="container py-5 mt-md-4" id="content">
        <div class="row justify-content-center text-center">
            <div class="col-12 col-md-10">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    the_content();
                    // End the loop.
                endwhile; ?>
            </div>
        </div>
    </div>
    <div class="container-fluid no-gutters">
        <?php
        $sections = get_field('sections');

        foreach ($sections as $section) {
            $title = $section['title'];
            $subtitle = $section['subtitle'];
            $lImage = $section['l_image'];
            $rImage = $section['r_image'];
            $lContent = $section['l_content'];
            $rContent = $section['r_content']; ?>
            <div class="row justify-content-center text-center">
                <div class="col-12 col-md-6">
                    <?php if (!empty($title)) { ?>
                        <h2 class="section-title mb-5"><?php echo $title; ?></h2>
                    <?php } ?>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-8 text-center">
                    <?php if (!empty($subtitle)) { ?>
                        <div class="section-subtitle"><?php echo $subtitle; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="row justify-content-center align-items-center mb-5">
                <?php if (!empty($lImage) || !empty($lContent)) { ?>
                    <div class="col-12 col-md-6 <?php if(!empty($lImage)) echo 'px-0'; else echo 'order-2 order-md-1' ?>">
                        <?php if (!empty($lImage)) { ?>
                            <img src="<?php echo $lImage; ?>" class="full-width" alt="">
                        <?php }
                        if (!empty($lContent)) { ?>
                       <div class="p-5">
                           <?php echo $lContent; ?>
                       </div>
                        <?php } ?>
                    </div>
                <?php }  if (!empty($rImage) || !empty($rContent)) { ?>
                    <div class="col-12 col-md-6 <?php if(!empty($rImage)) echo 'px-0 order-1 order-md-2' ?>">
                        <?php if (!empty($rImage)) { ?>
                            <img src="<?php echo $rImage; ?>" class="full-width" alt="">
                        <?php }
                        if (!empty($rContent)) { ?>
                            <div class="p-5">
                                <?php echo $rContent; ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
<?php get_footer(); ?>
