<?php
/*Template name: Home*/

get_header();
?>
<?php
// vars
$tr_bg = get_field('tr_image');
?>
<div class="page-home">
    <div class="container-fluid px-0 page-header">
        <div class="row no-gutters">
            <?php
            // vars
            $tl_bg = get_field('tl_image');
            $tl_bgc = get_field('tl_color');
            $tl_link = get_field('tl_link');
            $tl_gif = get_field('tl_gif');

            $tr_bg = get_field('tr_image');
            $tr_bgc = get_field('tr_color');
            $tr_link = get_field('tr_link');
            ?>
            <div class="col-12 col-md-6 " style="<?php if (!empty($tl_bgc)) echo 'background-color:' . $tl_bgc . '' ?>">
                <div class="position-relative">
                    <?php if (!empty($tl_bg)) { ?>
                        <img src="<?php echo $tl_bg; ?>" alt="" class="page-header__image">
                    <?php }

                    if (!empty($tl_link['title'])) { ?>
                        <h2 class="section-title">
                            <a href="<?php echo $tl_link['url']; ?>" target="<?php $tl_link['target']; ?>">
                                <?php echo $tl_link['title']; ?>
                            </a>
                        </h2>
                    <?php } ?>
                </div>
            </div>
            <div class="col-12 col-md-6 position-relative"
                 style="<?php if (!empty($tr_bgc)) echo 'background-color:' . $tr_bgc . '' ?>">
                <div class="position-relative">
                    <?php if (!empty($tr_bg)) { ?>
                        <img src="<?php echo $tr_bg; ?>" alt="" class="page-header__image">
                    <?php }

                    if (!empty($tr_link['title'])) { ?>
                        <h2 class="section-title">
                            <a href="<?php echo $tr_link['url']; ?>" target="<?php $tr_link['target']; ?>">
                                <?php echo $tr_link['title']; ?>
                            </a>
                        </h2>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="masonry-blocks">
        <?php $blocks = get_field('blocks');

        foreach ($blocks as $i => $block) {
            $bgc = $block['bg_color'];
            $bgi = $block['bg_image'];
            $title = $block['title'];
            $img = $block['image'];
            $color = $block['color'];
            $description = $block['description'];
            $link = $block['link'];
            $more = $block['link_text'];
            $gif = $block['gif']; ?>
            <div class="block <?php if (empty($img)) echo 'p-0' ?>"
                 style="<?php if (!empty($bgc)) echo 'background-color:' . $bgc . '' ?>">
                <?php
                if (!empty($gif['gif'])) { ?>
                    <a href="<?php echo $gif['link']; ?>" class="d-block block__gif">
                        <img src="<?php echo $gif['gif']; ?>" alt="" class="full-width">
                    </a>
                <?php }
                ?>
              <?php if(empty($description) && !empty($img) ) echo '<div class="center-content">'; ?>
                    <a href="<?php if (!empty($link)) echo $link; ?>" class="d-block" style="color: <?php echo $color; ?>">
                        <?php
                        if (!empty($bgi)) { ?>
                            <img src="<?php echo $bgi; ?>"
                                 class="block__bg-image <?php if (empty($img)) echo 'block__bg-image--main' ?> " alt="">
                        <?php }

                        if (!empty($title)) { ?>
                            <style>
                                .section-title.title-<?php echo $i; ?>:after {
                                    background-color: <?php echo $color; ?> !important;
                                }
                            </style>
                            <h2 class="section-title section-title--center title-<?php echo $i; ?>"><?php echo $title; ?></h2>
                        <?php }
                        if (!empty($img)) { ?>
                            <img src="<?php echo $img; ?>"
                                 class="block__image <?php if (!empty($description)) echo 'block__image--middle' ?><?php if(!empty($more)) echo 'block__image--more' ?>" alt="">
                        <?php }
                        if (!empty($description)) { ?>
                            <div class="block__description"><?php echo $description ?></div>
                        <?php }
                        if (!empty($more)) { ?>
                            <div class="block__more"><?php echo $more; ?></div>
                        <?php } ?>
                    </a>

                <?php if(empty($description) && !empty($img)) echo '</div>'; ?>
            </div>
        <?php } ?>
    </div>
</div>

<?php get_footer(); ?>
