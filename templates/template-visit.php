<?php
/*Template name: Visit us*/

get_header(); ?>
<div class="page-visit">
    <?php if (wp_is_mobile()) { ?>
        <div class="modal-map">
            <div class="close-map">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/close.svg" alt="">
            </div>
            <div id="map"></div>
        </div>
    <?php } ?>
    <div class="container-fluid px-0 page-header">
        <div class="row no-gutters">
            <?php
            // vars
            $hImage = get_field('h_image');
            $hColor = get_field('h_bg_color');
            $hTitle = get_field('h_title');
            ?>
            <div class="col-12 col-md-6 order-2 order-md-1">
                <div class="position-relative">
                    <?php if (!empty($hImage)) { ?>
                        <img src="<?php echo $hImage; ?>" alt="" class="page-header__image">
                    <?php } ?>
                </div>
            </div>
            <div class="col-12 col-md-6 position-relative mobile-height order-1 order-md-2"
                 style="<?php if (!empty($hColor)) echo 'background-color:' . $hColor . '' ?>">

                <?php
                if (!empty($hTitle['text'])) { ?>
                    <h1 class="section-title" style="color: <?php echo $hTitle['color']; ?>">
                        <?php echo $hTitle['text']; ?>
                    </h1>
                <?php } ?>
                <a href="#content" class="scroll-down">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.199" height="28.799" viewBox="0 0 25.199 28.799">
                        <g id="arrow-down" transform="translate(-5.401 -3.6)">
                            <path id="Path_17" data-name="Path 17"
                                  d="M30.073,18.527a1.8,1.8,0,0,1,0,2.545l-10.8,10.8a1.8,1.8,0,0,1-2.545,0l-10.8-10.8a1.8,1.8,0,1,1,2.545-2.545L16.2,26.255V5.4a1.8,1.8,0,0,1,3.6,0V26.255l7.727-7.727a1.8,1.8,0,0,1,2.545,0Z"
                                  fill="#fff" fill-rule="evenodd"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="container py-5 mt-md-4" id="content">
        <div class="row justify-content-center text-center">
            <div class="col-12 col-md-10">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    the_content();
                    // End the loop.
                endwhile; ?>
            </div>
        </div>
    </div>
    <div class="container-fluid mb-5 px-md-5">
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    <?php

                    $map = array();
                    $cats = get_terms('places', array(
                        'parent' => 0
                    ));
                    foreach ($cats as $i => $cat) { ?>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link <?php if ($i == 0) echo 'active'; ?>" id="<?php echo $cat->slug; ?>-tab"
                               data-toggle="tab" href="#<?php echo $cat->slug; ?>" role="tab"
                               aria-controls="<?php echo $cat->slug; ?>"
                               aria-selected="true"><?php echo $cat->name; ?></a>
                        </li>
                    <?php } ?>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <?php foreach ($cats as $j => $cat) {
                        $catID = $cat->term_id;
                        $cat_name = $cat->taxonomy;
                        $cat_children = get_term_children($catID, $cat_name); ?>
                        <div class="tab-pane py-4 fade <?php if ($j == 0) echo 'show active'; ?>"
                             id="<?php echo $cat->slug; ?>" role="tabpanel"
                             aria-labelledby="<?php echo $cat->slug; ?>-tab">
                            <div class="d-none d-md-inline-block">
                                <div class="select-container mr-md-4">
                                    <label for="filter">SUBCATEGORY:</label>
                                    <select name="filter" id="filter" data-id="" data-type=""
                                            class="places-filter">
                                        <option class="places-filter__option" value="<?php echo $cat->slug; ?>"
                                                data-id="<?= $catID; ?>"
                                                data-type="place"><?php echo $cat->name; ?> </option>
                                        <?php foreach ($cat_children as $child) {
                                            $term = get_term_by('id', $child, $cat_name); ?>
                                            <option class="places-filter__option" value="<?php echo $term->slug ?>"
                                                    data-id="<?= $term->term_id; ?>"
                                                    data-type="place"><?php echo $term->name; ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="d-block d-md-none">
                                <div class="filter-accordion" id="filterAccordion">
                                    <div>
                                        <div id="headingOne">
                                            <h2 class="mb-0">
                                                <button class="btn btn-block text-left" type="button"
                                                        data-toggle="collapse" data-target="#collapseOne"
                                                        aria-expanded="true" aria-controls="collapseOne">
                                                    <span>FILTER:</span> <span class="currentFilter"></span> <img
                                                            src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/arrow-down.svg"
                                                            class="float-right"/>
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                             data-parent="#filterAccordion">
                                            <ul class="mobile-filter" data-order="asc" data-orderby="name" data-id="">
                                                <?php foreach ($cat_children as $i => $child) {
                                                    $term = get_term_by('id', $child, $cat_name);
                                                    $pin = get_field('pin', $term); ?>
                                                    <li class="mobile-filter__item<?php if ($i == 0) echo ' active'; ?>"
                                                        data-id="<?= $term->term_id; ?>" data-order="asc"
                                                        data-orderby="name">
                                                        <img src="<?php echo $pin; ?>"
                                                             alt=""> <?php echo $term->name; ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                                <div class="select-container d-block d-md-none">
                                    <hr/>
                                    <label for="sort">SORTY BY:</label>
                                    <select name="sort" id="sort" data-order="asc" data-orderby="name" data-type="place"
                                            data-id="<?php echo $catID; ?>"
                                            class="places-sort">
                                        <option value="a-z" data-order="asc" data-orderby="name"
                                                class="places-sort__option">Alphabetical
                                            A-Z
                                        </option>
                                        <option value="z-a" data-order="desc" data-orderby="name"
                                                class="places-sort__option ">Alphabetical
                                            Z-A
                                        </option>
                                        <option value="date-asc" data-order="asc" data-orderby="date"
                                                class="places-sort__option ">By Date
                                            Ascending
                                        </option>
                                        <option value="date-desc" data-order="desc" data-orderby="date"
                                                class="places-sort__option">By Date
                                            Descending
                                        </option>
                                    </select>

                                </div>
                            </div>
                            <?php if (!wp_is_mobile()) {
                                foreach ($cat_children as $child) {
                                    $term = get_term_by('id', $child, $cat_name);

                                    if ($catID == 49) {
                                        $args = array(
                                            'post_type' => 'place',
                                            'post_status' => 'publish',
                                            'order' => 'asc',
                                            'orderby' => 'name',
                                            'posts_per_page' => -1,
                                            'nopaging' => true,
                                            'tax_query' => array(
                                                'relation' => 'OR',
                                                array(
                                                    'taxonomy' => 'places',
                                                    'field' => 'term_id',
                                                    'terms' => $term->term_id,
                                                ),
                                            ),
                                        );
                                        $places = new WP_Query($args);

                                        if ($places->have_posts()) {
                                            while ($places->have_posts()) {
                                                $places->the_post();
                                                $count = $places->found_posts;

                                                $terms = get_the_terms(get_the_ID(), 'places');

                                                if (!empty(get_field('pin', $terms[0]))) {
                                                    $pin = get_field('pin', $terms[0]);
                                                } else {
                                                    $pin = 'http://caleavictoriei.dev1.neurony.ro/wp-content/uploads/2020/11/bars-new.svg';
                                                }

                                                if (!empty(get_field('map', get_the_ID()))) {
                                                    $location = get_field('map', get_the_ID());
                                                } else {
                                                    $location = array(
                                                        'address' => 'Ion Brezoianu 23-25',
                                                        'lat' => floatval(95.45),
                                                        'lng' => floatval(96.26),
                                                        'zoom' => 15,
                                                        'place_id' => 'hIJ25QM-0P_sUAR9faY7Qs3nPE',
                                                        'street_number' => "23",
                                                        'street_name' => 'Strada Ion Brezoianu',
                                                        'city' => 'București',
                                                        'state' => 'București',
                                                        'post_code' => '030167',
                                                        'country' => 'Romania',
                                                        'country_short' => 'RO',
                                                    );
                                                }

                                                $phone = get_field('phone', get_the_ID());

                                                $map[] = array(
                                                    'title' => get_the_title(),
                                                    'address' => $location['address'],
                                                    'lat' => $location['lat'],
                                                    'lng' => $location['lng'],
                                                    'phone' => $phone,
                                                    'pin' => $pin,
                                                );
                                            }
                                        }
                                    }
                                }
                            } else { ?>
                                <div class="mobile-places" data-tab="<?php echo $catID; ?>">
                                    <?php foreach ($cat_children as $child) {
                                        $term = get_term_by('id', $child, $cat_name);

                                        if ($term->term_id == 54) {
                                            $args = array(
                                                'post_type' => 'place',
                                                'post_status' => 'publish',
                                                'order' => 'asc',
                                                'orderby' => 'name',
                                                'posts_per_page' => -1,
                                                'nopaging' => true,
                                                'tax_query' => array(
                                                    'relation' => 'OR',
                                                    array(
                                                        'taxonomy' => 'places',
                                                        'field' => 'term_id',
                                                        'terms' => $term->term_id,
                                                    ),
                                                ),
                                            );
                                            $places = new WP_Query($args);

                                            if ($places->have_posts()) {
                                                while ($places->have_posts()) {
                                                    $places->the_post();
                                                    $index = $places->current_post;

                                                    $terms = get_the_terms(get_the_ID(), 'places');
                                                    if (!empty(get_field('pin', $terms[0]))) {
                                                        $pin = get_field('pin', $terms[0]);
                                                    } else {
                                                        $pin = 'http://caleavictoriei.dev1.neurony.ro/wp-content/uploads/2020/11/bars-new.svg';
                                                    }
                                                    if (!empty(get_field('map', get_the_ID()))) {
                                                        $location = get_field('map', get_the_ID());
                                                    } else {
                                                        $location = array(
                                                            'address' => 'Ion Brezoianu 23-25',
                                                            'lat' => floatval(95.45),
                                                            'lng' => floatval(96.26),
                                                            'zoom' => 15,
                                                            'place_id' => 'hIJ25QM-0P_sUAR9faY7Qs3nPE',
                                                            'street_number' => "23",
                                                            'street_name' => 'Strada Ion Brezoianu',
                                                            'city' => 'București',
                                                            'state' => 'București',
                                                            'post_code' => '030167',
                                                            'country' => 'Romania',
                                                            'country_short' => 'RO',
                                                        );
                                                    }
                                                    $phone = get_field('phone', get_the_ID());

                                                    $map[] = array(
                                                        'title' => get_the_title(),
                                                        'address' => $location['address'],
                                                        'lat' => $location['lat'],
                                                        'lng' => $location['lng'],
                                                        'phone' => $phone,
                                                        'pin' => $pin,
                                                    );

                                                    get_template_part('parts/part', 'mobile_list', array('index' => $index));
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php }
                    if (!wp_is_mobile()) { ?>
                        <div id="map"></div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/filters.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuDkks5RkO1J3xv51q5iKjxI7kycUsFHY&callback=initMap&libraries=&v=weekly"
        defer
></script>
<script>
    let map;
    let locations = <?php echo json_encode($map); ?>;

    jQuery(function ($) {

        $('.places-filter').change(function () {
            let optionSelected = $("option:selected", this);

            $(this).data('id', $(optionSelected).data('id')).data('type', $(optionSelected).data('type'));

            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                dataType: 'html',
                data: {
                    action: 'filter_map',
                    term_id: $(this).data('id'),
                },
                success: function (res) {
                    let ajaxLocations = []
                    ajaxLocations.push(res);
                    locations = [];

                    if (ajaxLocations[0] !== 'empty') {
                        let split = ajaxLocations[0].split(/(?<=\})/);
                        for (let i = 0; i < split.length; i++) {
                            locations.push(JSON.parse(split[i]));
                        }
                        initMap();
                    }
                    initMap();
                },

            });

            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                dataType: 'html',
                data: {
                    action: 'filter_number',
                    term_id: $(this).data('id'),
                },
                success: function (res) {
                    $('#locations').html(res);
                },
            });
        });
    });


    $('.tab-pane').each(function () {
        let $this = $(this);
        $this.find('.mobile-filter__item').each(function () {
            $this.find('.currentFilter').text($this.find('.active').text());
            $(this).click(function () {
                $('#currentFilter').text($(this).text());
                $this.find('.mobile-filter__item').removeClass('active');
                $(this).addClass('active');

                $(this).parent().data('order', $('.places-sort').data('order')).data('orderby', $('.places-sort').data('orderby')).data('id', $(this).data('id'));
                $.ajax({
                    type: 'POST',
                    url: '/wp-admin/admin-ajax.php',
                    dataType: 'html',
                    data: {
                        action: 'filter_mobile',
                        term_id: $(this).parent().data('id'),
                        order: $(this).parent().data('order'),
                        orderby: $(this).parent().data('orderby'),
                    },
                    success: function (data) {
                        $this.find('.mobile-places').html(data);
                        let ajaxLocations = []
                        locations = [];

                        ajaxLocations.push($('.d-none.ajax').text());

                        let split = ajaxLocations[0].split(/(?<=\])/);

                        for (let i = 0; i < split.length - 1; i++) {
                            let remove1 = split[i].replace(/\[/g, '');
                            let remove2 = remove1.replace(/]/g, '');
                            locations.push(JSON.parse(remove2));
                        }

                        initMap();
                    },
                });

            });
        });
    });
</script>
<script>
    function initMap() {

        map = new google.maps.Map(document.getElementById("map"), {
            center: {lat: 44.4419771, lng: 26.0920451},
            zoom: 14,
        });
        let infowindow = [];
        let marker = [];
        for (let i = 0; i < locations.length; i++) {
            // Create popup
            infowindow[i] = new google.maps.InfoWindow({
                content: '<div class="map-window" id="' + i + '">' + locations[i]["title"] + '<br/>' + locations[i]['phone'] + '<br/>' + locations[i]['address'] + '</div>',
            });

            // Create marker.

            marker[i] = new google.maps.Marker({
                position: {lat: locations[i]['lat'], lng: locations[i]['lng']},
                map,
                icon: locations[i]['pin'],
            });

            marker[i].addListener("click", () => {
                infowindow[i].open(map, marker[i]);
            });

            $('.d-none.ajax').each(function () {
                $(this).parent().find('.open-map').click(function () {
                    infowindow[i].close(map, marker);
                    if (parseInt($(this).attr('id')) === i) {
                        infowindow[i].open(map, marker[i]);
                    }
                })
            })
        }
    }


</script>
<?php get_footer(); ?>
