<footer class="py-5 px-3">
    <div class="container-fluid">
        <div class="row py-lg-5 pb-5">
            <div class="col-12 col-lg-1 offset-lg-1 text-left mb-5 mb-lg-0">
                <?php $sLogo = get_field('secondary_logo', 'option'); ?>
                <a href="<?php echo home_url(); ?>">
                    <img src="<?php echo $sLogo; ?>" alt="" class="footer-logo">
                </a>
            </div>
            <div class="col-12 col-md-4 col-lg-2 footer-menu mb-4 mb-md-0">
                <?php
                if (is_active_sidebar('footer_explore')) {
                    dynamic_sidebar('footer_explore');
                } ?>
            </div>
            <div class="col-12 col-md-4 col-lg-2 footer-menu mb-4 mb-md-0">
                <?php
                if (is_active_sidebar('footer_about')) {
                    dynamic_sidebar('footer_about');
                } ?>
            </div>
            <div class="col-12 col-md-4 col-lg-2 footer-menu mb-4 mb-md-0">
                <?php
                if (is_active_sidebar('footer_follow')) {
                    dynamic_sidebar('footer_follow');
                } ?>
            </div>
            <div class="col-12 col-lg-4">
                <?php if (is_active_sidebar('footer_subscribe')) {
                    dynamic_sidebar('footer_subscribe');
                } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-6 offset-lg-2"><?php echo get_field('copyright', 'option'); ?></div>
        </div>
    </div>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>


<?php wp_footer(); ?>

