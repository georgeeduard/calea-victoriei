<?php

add_theme_support('menus');

register_nav_menus(array(
    'primary' => 'Menu',
    'mobile' => 'Mobile Menu'
));

add_theme_support('post-thumbnails');

if (function_exists('register_sidebar')) {

    register_sidebar(array(
        'name' => 'Footer Explore',
        'id' => 'footer_explore',
        'description' => 'Explore menu',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

    register_sidebar(array(
        'name' => 'Footer About Us',
        'id' => 'footer_about',
        'description' => 'About us Menu',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

    register_sidebar(array(
        'name' => 'Footer Follow Us',
        'id' => 'footer_follow',
        'description' => 'Follow Us Menu',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

    register_sidebar(array(
        'name' => 'Footer Subscribe',
        'id' => 'footer_subscribe',
        'description' => 'Mailchimp Form here',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));
}


function include_custom_jquery()
{
    wp_deregister_script('jquery');
    wp_deregister_script('jquery-migrate');

    wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js', true);
    wp_enqueue_script('jquery-migrate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.3.1/jquery-migrate.min.js', true);
}

add_action('wp_enqueue_scripts', 'include_custom_jquery');

if (!function_exists('places_function')) {

// Register Places
    function places_function()
    {
        $labels = array(
            'name' => _x('Places', 'Post Type General Name', 'text_domain'),
            'singular_name' => _x('Place', 'Post Type Singular Name', 'text_domain'),
            'menu_name' => __('Places', 'text_domain'),
            'name_admin_bar' => __('Place', 'text_domain'),
            'archives' => __('Place Archives', 'text_domain'),
            'attributes' => __('Place Attributes', 'text_domain'),
            'parent_item_colon' => __('Parent Item:', 'text_domain'),
            'all_items' => __('All Places', 'text_domain'),
            'add_new_item' => __('Add New PLace', 'text_domain'),
            'add_new' => __('Add New', 'text_domain'),
            'new_item' => __('New Place', 'text_domain'),
            'edit_item' => __('Edit Place', 'text_domain'),
            'update_item' => __('Update Place', 'text_domain'),
            'view_item' => __('View Place', 'text_domain'),
            'view_items' => __('View Places', 'text_domain'),
            'search_items' => __('Search Places', 'text_domain'),
            'not_found' => __('Not found', 'text_domain'),
            'not_found_in_trash' => __('Not found in Trash', 'text_domain'),
            'featured_image' => __('Featured Image', 'text_domain'),
            'set_featured_image' => __('Set featured image', 'text_domain'),
            'remove_featured_image' => __('Remove featured image', 'text_domain'),
            'use_featured_image' => __('Use as featured image', 'text_domain'),
            'insert_into_item' => __('Insert into item', 'text_domain'),
            'uploaded_to_this_item' => __('Uploaded to this place', 'text_domain'),
            'items_list' => __('Places list', 'text_domain'),
            'items_list_navigation' => __('Places list navigation', 'text_domain'),
            'filter_items_list' => __('Filter places list', 'text_domain'),
        );
        $args = array(
            'label' => __('Place', 'text_domain'),
            'description' => __('Places', 'text_domain'),
            'labels' => $labels,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'revisions',
                'custom-fields',
                'page-attributes',
                'excerpt'
            ),
            'taxonomies' => array('category', 'post_tag', 'places'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'show_in_rest' => true,
            'menu_position' => 10,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'page',
        );
        register_post_type('place', $args);
    }

    add_action('init', 'places_function', 0);

    // Register Places Taxonomy

    function places()
    {
        $labels = array(
            'name' => 'Places Categories',
            'singular_name' => 'Places Category',
            'menu_name' => 'Places Categories',
            'all_items' => 'All Items',
            'parent_item' => 'Parent Item',
            'parent_item_colon' => 'Parent Item:',
            'new_item_name' => 'New Item Name',
            'add_new_item' => 'Add New Item',
            'edit_item' => 'Edit Item',
            'update_item' => 'Update Item',
            'view_item' => 'View Item',
            'separate_items_with_commas' => 'Separate items with commas',
            'add_or_remove_items' => 'Add or remove items',
            'choose_from_most_used' => 'Choose from the most used',
            'popular_items' => 'Popular Items',
            'search_items' => 'Search Items',
            'not_found' => 'Not Found',
            'no_terms' => 'No items',
            'items_list' => 'Items list',
            'items_list_navigation' => 'Items list navigation',
        );

        $args = array(
            'labels' => $labels,
            'hierarchical' => true,
            'public' => true,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'show_in_rest' => true,
            'show_tagcloud' => false,
        );

        register_taxonomy('places', array('place'), $args);
    }

    add_action('init', 'places', 0);
}


function get_excerpt($count)
{
    $permalink = get_permalink($post->ID);
    $excerpt = get_the_content();
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = '<p>' . $excerpt . '...</p>';
    return $excerpt;
}

function get_my_excerpt($type, $count)
{
    get_the_content($more_link_text = null, $strip_teaser = false, $post = null);
    $excerpt = get_the_content(null, false, $type);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = '<p>' . $excerpt . '(...)</p>';
    return $excerpt;
}

if (function_exists('acf_add_options_page')) {
    acf_add_options_page();
}

// filter places

function filter_places()
{
    $postType = $_POST['type'];
    $cat_id = $_POST['term_id'];
    $order = $_POST['order'];
    $orderby = $_POST['orderby'];

    $args = array(
        'post_type' => $postType,
        'post_status' => 'publish',
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => -1,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'places',
                'field' => 'term_id',
                'terms' => $cat_id,
            ),
        ),
    );

    $ajaxposts = new WP_Query($args);

    $response = '';

    if ($ajaxposts->have_posts()) {
        while ($ajaxposts->have_posts()) : $ajaxposts->the_post();
            $response .= get_template_part('parts/part', 'place');
        endwhile;
    } else {
        $response = '<div class="col-12"><h3>We\'re sorry, there are no places for this selection</h3></div>';
    }

    echo $response;

    exit;
}

add_action('wp_ajax_filter_places', 'filter_places');
add_action('wp_ajax_nopriv_filter_places', 'filter_places');




// FIlter map locations

// filter number

function filter_number()
{
    $cat_id = $_POST['term_id'];

    $args = array(
        'post_type' => 'place',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'nopaging' => true,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'places',
                'field' => 'term_id',
                'terms' => $cat_id,
            ),
        ),
    );

    $ajaxposts = new WP_Query($args);

    $response = '';

    if ($ajaxposts->have_posts()) {
        while ($ajaxposts->have_posts()) : $ajaxposts->the_post();
            $count = $ajaxposts->found_posts;

            $response = $count;
        endwhile;
    } else {
        $response = 'empty';
    }

    echo $response;

    exit;
}

add_action('wp_ajax_filter_number', 'filter_number');
add_action('wp_ajax_nopriv_filter_number', 'filter_number');

function filter_map()
{
    $cat_id = $_POST['term_id'];

    $args = array(
        'post_type' => 'place',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'nopaging' => true,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'places',
                'field' => 'term_id',
                'terms' => $cat_id,
            ),
        ),
    );

    $ajaxposts = new WP_Query($args);

    $response = '';

    if ($ajaxposts->have_posts()) {
        while ($ajaxposts->have_posts()) : $ajaxposts->the_post();
            $count = $ajaxposts->found_posts; ?>

            <?php
            $terms = get_the_terms(get_the_ID(), 'places');
            if(!empty(get_field('pin', $terms[0]))) {
                $pin = get_field('pin', $terms[0]);
            }
            else {
                $pin = 'http://caleavictoriei.dev1.neurony.ro/wp-content/uploads/2020/11/bars-new.svg';
            }
            if (!empty(get_field('map', get_the_ID()))) {
                $location = get_field('map', get_the_ID());
            } else {
                $location = array(
                    'address' => 'Ion Brezoianu 23-25',
                    'lat' => floatval(95.45),
                    'lng' => floatval(96.26),
                    'zoom' => 15,
                    'place_id' => 'hIJ25QM-0P_sUAR9faY7Qs3nPE',
                    'street_number' => "23",
                    'street_name' => 'Strada Ion Brezoianu',
                    'city' => 'București',
                    'state' => 'București',
                    'post_code' => '030167',
                    'country' => 'Romania',
                    'country_short' => 'RO',
                );
            }
            $phone = get_field('phone', get_the_ID());

            $map = array(
                'title' => get_the_title(),
                'address' => $location['address'],
                'lat' => $location['lat'],
                'lng' => $location['lng'],
                'phone' => $phone,
                'pin' => $pin,
            );
            $response .= json_encode($map);
        endwhile;
    } else {
        $response = 'empty';
    }

    echo $response;

    exit;
}

add_action('wp_ajax_filter_map', 'filter_map');
add_action('wp_ajax_nopriv_filter_map', 'filter_map');

add_filter('posts_where', 'title_filter', 10, 2);

function title_filter($where, &$wp_query)
{
    global $wpdb;
    if ($search_term = $wp_query->get('search_prod_title')) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql($wpdb->esc_like($search_term)) . '%\'';
    }
    return $where;
}


function filter_mobile()
{

    $cat_id = $_POST['term_id'];
    $order = $_POST['order'];
    $orderby = $_POST['orderby'];

    $args = array(
        'post_type' => 'place',
        'post_status' => 'publish',
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => -1,
        'nopaging' => true,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'places',
                'field' => 'term_id',
                'terms' => $cat_id,
            ),
        ),
    );

    $ajaxposts = new WP_Query($args);

    $response = '';

    if ($ajaxposts->have_posts()) {
        while ($ajaxposts->have_posts()) : $ajaxposts->the_post();



       $index = $ajaxposts->current_post;

            $response .= get_template_part('parts/part', 'mobile_list', array ('index'=> $index));
        endwhile;
    } else {
        $response = '<div class="col-12"><h3>We\'re sorry, there are no places for this selection</h3></div>';
    }

    echo $response;

    exit;
}

add_action('wp_ajax_filter_mobile', 'filter_mobile');
add_action('wp_ajax_nopriv_filter_mobile', 'filter_mobile');


// Sort posts

function sort_articles()
{
    $order = $_POST['order'];
    $orderby = $_POST['orderby'];

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => -1,
    );

    $ajaxposts = new WP_Query($args);

    $response = '';

    if ($ajaxposts->have_posts()) {
        while ($ajaxposts->have_posts()) : $ajaxposts->the_post();
            $response .= get_template_part('parts/part', 'content');
        endwhile;
    } else {
        $response = '<div class="col-12"><h3>We\'re sorry, there are no articles</h3></div>';
    }

    echo $response;

    exit;
}

add_action('wp_ajax_sort_articles', 'sort_articles');
add_action('wp_ajax_nopriv_sort_articles', 'sort_articles');

function sort_mobile()
{

    $cat_id = $_POST['term_id'];
    $order = $_POST['order'];
    $orderby = $_POST['orderby'];

    $args = array(
        'post_type' => 'place',
        'post_status' => 'publish',
        'order' => $order,
        'orderby' => $orderby,
        'posts_per_page' => -1,
        'nopaging' => true,
        'tax_query' => array(
            'relation' => 'OR',
            array(
                'taxonomy' => 'places',
                'field' => 'term_id',
                'terms' => $cat_id,
            ),
        ),
    );

    $ajaxposts = new WP_Query($args);

    $response = '';

    if ($ajaxposts->have_posts()) {
        while ($ajaxposts->have_posts()) : $ajaxposts->the_post();

            $terms = get_the_terms(get_the_ID(), 'places');
            if(!empty(get_field('pin', $terms[0]))) {
                $pin = get_field('pin', $terms[0]);
            }
            else {
                $pin = 'http://caleavictoriei.dev1.neurony.ro/wp-content/uploads/2020/11/bars-new.svg';
            }
            $location = get_field('map', get_the_ID());
            $phone = get_field('phone', get_the_ID());

            $map[] = array(
                'title' => get_the_title(),
                'address' => $location['address'],
                'lat' => $location['lat'],
                'lng' => $location['lng'],
                'phone' => $phone,
                'pin' => $pin,
            );
            $response .= get_template_part('parts/part', 'mobile_list');
        endwhile;
    } else {
        $response = '<div class="col-12"><h3>We\'re sorry, there are no places for this selection</h3></div>';
    }

    echo $response;

    exit;
}

add_action('wp_ajax_sort_mobile', 'sort_mobile');
add_action('wp_ajax_nopriv_sort_mobile', 'sort_mobile');


add_action( 'admin_head', function () { ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuDkks5RkO1J3xv51q5iKjxI7kycUsFHY&callback=initMap" type="text/javascript"></script>
<?php } );

add_action('wp_enqueue_scripts', 'admin_head');