<?php get_header(); ?>
<div class="single-place single-post">
    <div class="container-fluid px-0 page-header">
        <div class="row no-gutters">
            <?php
            // vars
            $hColor = get_field('h_bg_color');
            $arrow = get_field('arrow_color');
            $tColor = get_field('t_color');
            ?>
            <div class="col-12 col-md-6 order-2 order-md-1">
                <div class="position-relative">

                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="page-header__image">

                </div>
            </div>
            <div class="col-12 col-md-6 position-relative mobile-height order-1 order-md-2"
                 style="<?php if (!empty($hColor)) echo 'background-color:' . $hColor . '' ?>">

                    <h1 class="section-title" style="color: <?php echo $tColor; ?>">
                        <?php echo the_title(); ?>
                    </h1>


                <a href="#content" class="scroll-down">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.199" height="28.799" viewBox="0 0 25.199 28.799">
                        <g id="arrow-down" transform="translate(-5.401 -3.6)">
                            <path id="Path_17" data-name="Path 17"
                                  d="M30.073,18.527a1.8,1.8,0,0,1,0,2.545l-10.8,10.8a1.8,1.8,0,0,1-2.545,0l-10.8-10.8a1.8,1.8,0,1,1,2.545-2.545L16.2,26.255V5.4a1.8,1.8,0,0,1,3.6,0V26.255l7.727-7.727a1.8,1.8,0,0,1,2.545,0Z"
                                  fill="<?php echo $arrow; ?>" fill-rule="evenodd"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="container py-5 mt-md-4" id="content">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                ?>
                <div class="post-date mb-4"><?php echo get_the_date(); ?></div>
                <?php
                    the_content();
                    // End the loop.
                endwhile; ?>
                <hr class="my-5">
                <div class="social-share">
                    <i class="fas fa-share-alt"></i> SHARE: <?php echo do_shortcode('[addtoany]'); ?>
                </div>
                <hr class="d-md-none">
            </div>

        </div>
    </div>

</div>

<?php get_footer(); ?>
