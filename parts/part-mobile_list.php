<div class="row mobile-list">
    <div class="d-none ajax"><?php
        global $index;
        $terms = get_the_terms(get_the_ID(), 'places');
        $pin = get_field('pin', $terms[0]);
        $location = get_field('map', get_the_ID());
        $phone = get_field('phone', get_the_ID());

        $map[] = array(
            'title' => get_the_title(),
            'address' => $location['address'],
            'lat' => $location['lat'],
            'lng' => $location['lng'],
            'phone' => $phone,
            'pin' => $pin,
        );
        echo json_encode($map); ?>
    </div>
    <div class="col-4">
        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="" class="mobile-list__img">
    </div>
    <div class="col-8 pl-0">
        <h2><?php echo the_title(); ?></h2>
        <div class="address"><?php echo get_field('address'); ?></div>
        <div class="phone"><?php echo get_field('phone'); ?></div>
        <a href="#" class="open-map" id="<?php echo $args['index'] ?>">
            <span class="d-inline-block" style="vertical-align: middle;">VIEW ON MAP</span>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/arrow-right-black.svg"
                 style="width: 15px; margin-left: .5rem" alt=""></a>
    </div>
    <div class="col-12">
        <hr/>
    </div>
</div>