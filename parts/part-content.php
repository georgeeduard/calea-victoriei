<div class="col-12 col-md-6 mb-4">
    <div class="place">
        <a href="<?php the_permalink(); ?>">
            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="place__img" alt="">
            <div class="place__content px-4">
                <h2 class="mb-3 place__title"><?php echo the_title(); ?>
                    <div class="place__date"><?php echo get_the_date(); ?></div></h2>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/arrow-right.svg"
                     alt="" class="arrow">

            </div>
        </a>
    </div>
</div>