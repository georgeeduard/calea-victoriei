jQuery(function ($) {
    $('.places-filter').change(function () {
        let optionSelected = $("option:selected", this);

        $(this).data('id', $(optionSelected).data('id')).data('type', $(optionSelected).data('type'));

        if ($('.places-sort').val() !== 0) {
            $('.places-sort').data('type', $(optionSelected).data('type')).data('id', $(optionSelected).data('id'));
        }

        $('.filter-text').text(optionSelected.text());

        if ($('#grid').hasClass('active')) {
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                dataType: 'html',
                data: {
                    action: 'filter_places',
                    term_id: $(this).data('id'),
                    type: $(this).data('type'),
                    order: $(this).data('order'),
                    orderby: $(this).data('orderby'),
                },
                success: function (response) {
                    $('.places').html(response);
                },
            });
        }
        /* else {
             $.ajax({
                 type: 'POST',
                 url: '/wp-admin/admin-ajax.php',
                 dataType: 'html',
                 data: {
                     action: 'filter_placesList',
                     term_id: $(this).data('id'),
                     type: $(this).data('type'),
                     order: $(this).data('order'),
                     orderby: $(this).data('orderby'),
                 },
                 success: function(res) {
                     $('.places--list').html(res);
                 },
             });
         }*/
    });

    $('.places-sort').change(function () {
        let optionSelected = $("option:selected", this);

        if ($(window).width() > 768) {
            $(this).data('order', $(optionSelected).data('order')).data('orderby', $(optionSelected).data('orderby'));
            $('.places-filter').data('order', $(optionSelected).data('order')).data('orderby', $(optionSelected).data('orderby'));
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                dataType: 'html',
                data: {
                    action: 'filter_places',
                    term_id: $(this).data('id'),
                    type: $(this).data('type'),
                    order: $(this).data('order'),
                    orderby: $(this).data('orderby'),
                },
                success: function (response) {
                    $('.places').html(response);
                },
            });

            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                dataType: 'html',
                data: {
                    action: 'sort_articles',
                    order: $(this).data('order'),
                    orderby: $(this).data('orderby'),
                },
                success: function (response) {
                    $('.articles').html(response);
                },
            });
        } else {
            $(this).data('order', $(optionSelected).data('order')).data('orderby', $(optionSelected).data('orderby')).data('id', $('.mobile-filter').data('id'));


            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                dataType: 'html',
                data: {
                    action: 'sort_mobile',
                    term_id: $(this).data('id'),
                    order: $(this).data('order'),
                    orderby: $(this).data('orderby'),
                },
                success: function (response) {
                    $('.mobile-places').html(response);
                },
            });
        }
    });
});


