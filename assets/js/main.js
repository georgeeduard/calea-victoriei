let rightHeight = 0;
let leftHeight = 0;
const $mainMenu = jQuery('#main-menu'),
    $showLeft = jQuery('#showLeft'),
    $close = jQuery('#menuClose');

jQuery(function ($) {
    $showLeft.click(function () {
        $mainMenu.addClass('container-open');
    });

    $close.click(function () {
        $mainMenu.removeClass('container-open');
    });


    setTimeout(function () {
        $('.block').each(function (index) {
            if (index % 2 !== 0) {
                rightHeight += $('.block').eq(index).outerHeight();
            } else {
                leftHeight += $('.block').eq(index).outerHeight();
            }
        })

        if (leftHeight > rightHeight) {
            $('.masonry-blocks').height(Math.ceil(leftHeight));
        } else {
            $('.masonry-blocks').height(Math.ceil(rightHeight));
        }

    }, 500);


    $('.view').each(function () {
        $(this).click(function () {
            $('.view').removeClass('active');
            $(this).addClass('active');

            if ($('#list').hasClass('active')) {
                $('.places-sort').parent().hide();
                $('.places-filter').parent().hide();
                $('#grid-view').hide();
                $('#list-view').show();
            } else {
                $('.places-sort').parent().show();
                $('.places-filter').parent().show();
                $('#list-view').hide();
                $('#grid-view').show();
            }
        });
    });

    $('.page-numbers.prev').text('PREV');
    $('.page-numbers.next').text('NEXT');

    $('#openSearch').click(function () {
        $('.search-modal').show();
        $mainMenu.removeClass('container-open');
    });

    $('#searchClose').click(function () {
        $('.search-modal').hide();
    });


    $('.has-content').each(function () {
        if ($(this).find('.place').length < 1) {
            $(this).remove();
        }

    });

    $('.close-map').click(function () {
        $('.modal-map').hide();
    });

    $(document).on('click','.open-map',function (evt) {
        evt.preventDefault();
        $('.modal-map').show();
    });

    if ($(window).width() > 767) {
        $('#myTab .nav-item .nav-link').each(function (i) {
            $(this).click(function () {
                let $this = $(this);
                setTimeout(function () {
                    if ($this.hasClass('active')) {
                        let $select = $('.tab-pane.active').find('select');
                        let $option = $select.find('option').eq(0);
                        $select.val($option.val()).trigger('change');
                    }
                }, 200);

            });
        });
    }

    else {
        $('#myTab .nav-item .nav-link').each(function (i) {
            $(this).click(function () {
                let $this = $(this);
                setTimeout(function () {
                    if ($this.hasClass('active')) {
                        let $active = $('.tab-pane.active').find('.mobile-filter__item.active');
                        $active.trigger('click');
                    }
                }, 200);

            });
        });
    }


    if($(window).width() > 991) {
        $('.page-header__image').height($(window).height());
        $('.block').height($(window).height());
    }

    else if($(window).width() >= 768 && $(window).width() <= 991) {
        $('.page-header__image').height($(window).height() * 0.45);
        $('.block').height($(window).height() * 0.45);

    }
});



