<?php get_header(); ?>

    <div class="container py-5">
        <div class="row my-5 pt-5 justify-content-center">
          <div class= "col-12 col-md-10 col-lg-8 pt-5">

                <?php
                // Start the loop.
                while (have_posts()) : the_post(); ?>
                    <h1 class="text-center mb-5"><?php echo the_title(); ?></h1>
                    <?php the_content();
                    // End the loop.
                endwhile; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>