<?php get_header();
$catID = get_queried_object()->term_id;
?>

<div class="page-category">
    <div class="container-fluid px-0 page-header">
        <div class="row no-gutters">
            <?php
            // vars
            $term = get_queried_object();
            $cImage = get_field('c_image', $term);
            $cColor = get_field('c_bg_color', $term);
            $cTitle = get_field('c_title', $term);
            $cArrow = get_field('c_arrow_color', $term);
            ?>
            <div class="col-12 col-md-6 order-2 order-md-1">
                <div class="position-relative">
                    <?php if (!empty($cImage)) { ?>
                        <img src="<?php echo $cImage; ?>" alt="" class="page-header__image">
                    <?php } ?>
                </div>
            </div>
            <div class="col-12 col-md-6 position-relative mobile-height order-1 order-md-2"
                 style="<?php if (!empty($cColor)) echo 'background-color:' . $cColor . '' ?>">
                <?php
                if (!empty($cTitle['text'])) { ?>
                    <h1 class="section-title" style="color: <?php echo $cTitle['color']; ?>">
                        <?php echo $cTitle['text']; ?>
                    </h1>
                <?php } ?>
                <a href="#content" class="scroll-down">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.199" height="28.799" viewBox="0 0 25.199 28.799">
                        <g id="arrow-down" transform="translate(-5.401 -3.6)">
                            <path id="Path_17" data-name="Path 17"
                                  d="M30.073,18.527a1.8,1.8,0,0,1,0,2.545l-10.8,10.8a1.8,1.8,0,0,1-2.545,0l-10.8-10.8a1.8,1.8,0,1,1,2.545-2.545L16.2,26.255V5.4a1.8,1.8,0,0,1,3.6,0V26.255l7.727-7.727a1.8,1.8,0,0,1,2.545,0Z"
                                  fill="<?php echo $cArrow; ?>" fill-rule="evenodd"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <div class="container py-5 mt-md-4" id="content">
        <div class="row justify-content-center text-center">
            <div class="col-12 col-md-10">
                <?php echo term_description(); ?>
                <hr class="my-5 d-none d-md-block">
            </div>
        </div>
    </div>
    <div class="container-fluid px-4">
        <div class="row mb-3">
            <div class="col-12 col-md-2 col-lg-3 mb-md-3 order-4 order-md-1">
                <hr class="d-md-none"/>
                <h3 class="filter-text mb-0">ALL <?php echo get_queried_object()->name; ?></h3>
            </div>
            <div class="col-12 col-md-4 mb-md-3 pl-md-0 order-md-2">

                <?php

                $cat_name = get_queried_object()->taxonomy;
                $cat_children = get_term_children($catID, $cat_name);

                // Display the children
                ?>
                <div class="select-container">
                    <hr class="d-md-none"/>
                    <label for="filter">FILTER:</label>
                    <select name="filter" id="filter" data-id="" data-type="" data-order="asc" data-orderby="name"
                            class="places-filter">
                        <option value="all" class="places-filter__option" data-id="<?php echo $catID; ?>">
                            All <?php echo get_queried_object()->name; ?></option>
                        <?php
                        foreach ($cat_children as $child) {
                            $term = get_term_by('id', $child, $cat_name); ?>
                            <option class="places-filter__option" value="<?php echo $term->slug ?>"
                                    data-id="<?= $term->term_id; ?>" data-type="place"><?php echo $term->name; ?> </option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-12 col-md-5 col-lg-4 mb-md-3 ml-auto order-md-3 pr-md-0">

                <div class="select-container float-md-right">
                    <hr class="d-md-none"/>
                    <label for="sort">SORTY BY:</label>
                    <select name="sort" id="sort" data-order="" data-orderby="" data-type="place" data-id="<?php echo $catID; ?>"
                            class="places-sort">
                        <option value="a-z" data-order="asc" data-orderby="name" class="places-sort__option">Alphabetical
                            A-Z
                        </option>
                        <option value="z-a" data-order="desc" data-orderby="name" class="places-sort__option ">Alphabetical
                            Z-A
                        </option>
                        <option value="date-asc" data-order="asc" data-orderby="date" class="places-sort__option ">By Date
                            Ascending
                        </option>
                        <option value="date-desc" data-order="desc" data-orderby="date" class="places-sort__option">By Date
                            Descending
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-12 col-md-1 mb-3 text-right order-md-4 px-md-0">
                <hr class="d-md-none"/>
                <div class="float-left d-md-none view-type ">VIEW TYPE:</div>
                <div id="grid" class="view active mr-3 mr-md-1 mr-lg-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                        <path id="Union_3" data-name="Union 3" d="M-1958,340v-9h9v9Zm-11,0v-9h9v9Zm11-11v-9h9v9Zm-11,0v-9h9v9Z" transform="translate(1969 -320)"/>
                    </svg>
                </div>
                <div id="list" class="view">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                        <path id="Union_2" data-name="Union 2" d="M-1969,340v-2h20v2Zm0-9v-2h20v2Zm0-9v-2h20v2Z" transform="translate(1969 -320)" opacity="0.5"/>
                    </svg>
                </div>

            </div>
        </div>
        <?php
        $paged = get_query_var("paged") ? get_query_var("paged") : 1;

        $args = array(
            'post_type' => 'place',
            'post_status' => 'publish',
            'order' => 'asc',
            'orderby' => 'name',
            'posts_per_page' => 6,
            'paged' => $paged,
            'tax_query' => array(
                'relation' => 'OR',
                array(
                    'taxonomy' => 'places',
                    'field' => 'term_id',
                    'terms' => $catID,
                ),
            ),
        );

        // The Query
        $places = new WP_Query($args); ?>
        <div class="row places" id="grid-view">
            <?php
            // The Loop
            if ($places->have_posts()) {
                while ($places->have_posts()) {
                    $places->the_post();
                    include('parts/part-place.php');
                }
            } else {
                ?>

                <div class="col-12">
                    <h3>We're sorry, there are no places for this selection</h3>
                </div>
            <?php }

            $big = 999999999; // need an unlikely integer

            // Restore original Post Data
            wp_reset_postdata();
            ?>
            <div class="col-12 py-5">
                <div class="places__pagination text-center">
                    <?php
                    echo paginate_links(array(
                        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
                        'format' => '?paged=%#%',
                        'current' => max(1, get_query_var('paged')),
                        'total' => $places->max_num_pages
                    ));
                    ?>
                </div>
            </div>
        </div>
        <div class="row places places--list" id="list-view" style="display: none">
            <?php foreach (range('A', 'Z') as $char) {

                echo '<div class="col-12 col-md-4 col-lg-3 mb-4 has-content">';
                echo '<div class="letter">'.$char.'</div>';

                $argsList = array(
                    'post_type' => 'place',
                    'post_status' => 'publish',
                    'order' => 'asc',
                    'orderby' => 'name',
                    'search_prod_title' => $char,
                    'posts_per_page' => -1,
                    'nopaging' => true,
                    'tax_query' => array(
                        'relation' => 'OR',
                        array(
                            'taxonomy' => 'places',
                            'field' => 'term_id',
                            'terms' => $catID,
                        ),
                    ),
                );

                $placesList = new WP_Query($argsList);
                if ($placesList->have_posts()) {
                    while ($placesList->have_posts()) {
                        $placesList->the_post();
                        $title= get_the_title();
                        $initial=strtoupper(substr($title,0,1));
                        if($initial == $char) {
                            include('parts/part-place_list.php');
                        }
                    }
                    wp_reset_postdata();

                }
                echo '</div>';
            }
            remove_filter( 'posts_where', 'title_filter', 10, 2 );

            ?>

        </div>
        <hr class="my-5">
    </div>
</div>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/filters.js"></script>
<?php get_footer(); ?>
