<?php get_header();

$s = get_search_query();
$args = array(
    's' => $s
); ?>

    <div class="page-search">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center local-search py-5">
                    <?php echo do_shortcode('[ivory-search id="426" title="Default Search Form"]'); ?>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">

                    <?php
                    // The Query
                    $the_query = new WP_Query($args);
                    if ($the_query->have_posts()) {
                        ?>
                        <!--<div class="col-12">
                            <h2 style='font-weight:bold;color:#000'>Search Results for: "<?php /*echo get_query_var('s'); */?> "</h2>

                        </div>-->
                        <?php
                        while ($the_query->have_posts()) {
                            $the_query->the_post();
                            get_template_part('parts/part', 'place');
                        }
                    } else {
                        ?>
                        <h2 style='font-weight:bold;color:#000'>Nothing Found</h2>
                        <div class="alert alert-info">
                            <p>Sorry, but nothing matched your search criteria. Please try again with some different
                                keywords.</p>
                        </div>
                    <?php } ?>

            </div>
        </div>
    </div>

<?php get_footer(); ?>