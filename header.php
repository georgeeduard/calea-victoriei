<!DOCTYPE html>
<html id="to-top">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo wp_title(); ?></title>

    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@500;700&display=swap" rel="stylesheet">
    <?php if (is_singular()) : ?>
        <script src="https://kit.fontawesome.com/7591d80950.js" crossorigin="anonymous"></script>
    <?php endif ?>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">

    <?php wp_head(); ?>
</head>
<body class="<?php if (is_admin_bar_showing()) echo 'is-admin' ?>">
<div class="search-modal">
    <div class="search-form">
        <?php echo do_shortcode('[ivory-search id="426" title="Default Search Form"]'); ?>
    </div>
    <div class="close-search">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/close.svg" alt="" id="searchClose">
    </div>
</div>
<header>
    <?php $logo = get_field('logo', 'option'); ?>
    <?php $fLogo = get_field('fixed_logo', 'option'); ?>
    <?php $sLogo = get_field('secondary_logo', 'option'); ?>
    <?php
    if (is_tax()) {
        $logo_color = get_field('logo_color', get_queried_object());
    } elseif (is_search()) {
        $logo_color = '#000';
    } else {
        $logo_color = get_field('logo_color');
    } ?>

    <nav class="menu-container " id="main-menu">
        <div class="full-width mb-5 mb-md-4">
            <img src="<?php echo $sLogo ?>" alt="" class="menu-logo">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/close.svg" alt="" id="menuClose"
                 class="close-menu ">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/search.svg" alt="" id="openSearch"
                 class="search-open">
        </div>
        <?php
        wp_nav_menu(
            array(
                'theme_location' => 'primary',
                'menu_class' => 'main-menu',
            )
        );
        ?>
    </nav>

    <div class="logo-container">
        <a href="<?php echo home_url(); ?>">
            <svg class="logo" xmlns="http://www.w3.org/2000/svg" width="197.048" height="121.881"
                 viewBox="0 0 197.048 121.881">
                <g id="Group_1" data-name="Group 1" transform="translate(0 0)">
                    <path id="Path_1" data-name="Path 1"
                          d="M0-57.875a8.012,8.012,0,0,1,8.188-8.141,6.94,6.94,0,0,1,6.364,3.252l-2.5,1.779a4.337,4.337,0,0,0-3.86-2.083c-2.877,0-4.819,2.223-4.819,5.194s1.942,5.241,4.819,5.241a4.534,4.534,0,0,0,4.094-2.339l2.55,1.754a7.486,7.486,0,0,1-6.644,3.509A8.031,8.031,0,0,1,0-57.875Zm131.175,0a8.057,8.057,0,0,1,8.235-8.165,8.057,8.057,0,0,1,8.235,8.165,8.056,8.056,0,0,1-8.235,8.165A8.056,8.056,0,0,1,131.175-57.875Zm-29.642,0a8.012,8.012,0,0,1,8.188-8.141,6.94,6.94,0,0,1,6.364,3.252l-2.5,1.779a4.336,4.336,0,0,0-3.86-2.083c-2.877,0-4.819,2.223-4.819,5.194s1.942,5.241,4.819,5.241a4.533,4.533,0,0,0,4.094-2.339l2.551,1.754a7.488,7.488,0,0,1-6.645,3.509A8.031,8.031,0,0,1,101.533-57.875ZM78.653-65.83h3.51L84.9-59.7c.562,1.287,1.171,3.392,1.24,3.6a28.393,28.393,0,0,1,1.217-3.6l2.667-6.13h3.182L86.234-49.78h-.327ZM196.471-49.87a.546.546,0,0,1-.55-.557.545.545,0,0,1,.55-.55.547.547,0,0,1,.557.55A.548.548,0,0,1,196.471-49.87ZM150.43-65.83h5.9c3.252,0,5.919,1.521,5.919,5.241a4.682,4.682,0,0,1-3.112,4.656l3.626,6.013h-3.743l-3.228-5.521H153.7v5.521H150.43Zm-28,2.878h-4.539V-65.83h12.353v2.878H125.7v13.031h-3.275ZM22.39-65.97h.281l7.276,16.049h-3.44l-.889-2.152H19.442l-.889,2.152H15.394Zm42.555,0h.281L72.5-49.921h-3.44l-.889-2.152H62l-.889,2.152H57.949ZM185.825-49.921H189.1V-65.83h-3.275Zm-20.611,0h3.275V-65.83h-3.275Zm-69.787,0H98.7V-65.83H95.428ZM45.222-65.83H55.75v2.878H48.5v3.65h6.388v2.808H48.5v3.7h7.58v2.877H45.222Zm126.636,0h10.528v2.878h-7.253v3.65h6.388v2.808h-6.388v3.7h7.58v2.877H171.859ZM32.1-65.83h3.275V-52.8h7.206v2.877H32.1ZM194.761-51.291a2.244,2.244,0,0,1-2.28-2.294,1.945,1.945,0,0,1,.911-1.782l.5.7a1.214,1.214,0,0,0-.583,1.081,1.351,1.351,0,0,0,1.455,1.35,1.362,1.362,0,0,0,1.467-1.35,1.269,1.269,0,0,0-.655-1.146l.492-.715a2.1,2.1,0,0,1,.982,1.861A2.249,2.249,0,0,1,194.761-51.291Zm-50.485-6.584c0-2.971-1.965-5.241-4.866-5.241s-4.866,2.269-4.866,5.241,1.965,5.241,4.866,5.241S144.276-54.9,144.276-57.875ZM24.448-54.811,23.442-57.22c-.444-1.053-.912-2.667-.959-2.831-.023.07-.491,1.731-.935,2.831l-1.006,2.409Zm42.555,0L66-57.22c-.444-1.053-.912-2.667-.959-2.831-.023.07-.491,1.731-.935,2.831L63.1-54.811Zm127.758-1.1a2.256,2.256,0,0,1-2.286-2.306,2.256,2.256,0,0,1,2.286-2.306,2.256,2.256,0,0,1,2.286,2.306A2.256,2.256,0,0,1,194.761-55.91Zm0-3.669a1.369,1.369,0,0,0-1.467,1.363,1.368,1.368,0,0,0,1.467,1.362,1.368,1.368,0,0,0,1.467-1.362A1.369,1.369,0,0,0,194.761-59.58ZM156.231-58.2c1.614,0,2.644-.631,2.644-2.387,0-1.778-1.076-2.362-2.644-2.362H153.7V-58.2Zm36.256-3.1v-.072l2.411-2.194-2.411-2.163v-.078h4.5v.884h-1.671c-.3,0-.78-.033-.78-.033a7.742,7.742,0,0,1,.629.478l.99.884v.086l-.99.884a7.742,7.742,0,0,1-.629.478s.479-.033.78-.033h1.671v.878ZM53.065-171.592H75.977l35.533,71.165-11.359,21.65Zm78.129,5.311a17.766,17.766,0,0,0-25.125,0,17.768,17.768,0,0,0,0,25.125l12.563,12.562,12.562-12.562A17.766,17.766,0,0,0,131.193-166.281Z"
                          transform="translate(0 171.592)" fill="<?php echo $logo_color; ?>"/>
                </g>
            </svg>
        </a>
        <?php if (is_front_page()) { ?>
            <a href="<?php echo home_url(); ?>">
                <svg class="logo--fixed" xmlns="http://www.w3.org/2000/svg" width="83.332" height="92.815"
                     viewBox="0 0 83.332 92.815">
                    <path id="Path_1" data-name="Path 1"
                          d="M65.866-171.592H88.778l35.533,71.165-11.359,21.65Zm78.129,5.311a17.766,17.766,0,0,0-25.125,0,17.768,17.768,0,0,0,0,25.125l12.563,12.562,12.562-12.562A17.766,17.766,0,0,0,143.995-166.281Z"
                          transform="translate(-65.866 171.592)" fill="<?php echo $logo_color; ?>"/>
                </svg>
            </a>
        <?php } ?>
    </div>
    <div class="menu-open">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/open.svg" alt="" id="showLeft">
    </div>
</header>

