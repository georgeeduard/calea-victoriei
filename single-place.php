<?php get_header(); ?>
<div class="single-place">
    <?php if (wp_is_mobile()) { ?>
        <div class="modal-map">
            <div class="close-map">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/svg/close.svg" alt=""></div>
            <div id="map"></div>
        </div>
    <?php } ?>
    <div class="container-fluid px-0 page-header">
        <div class="row no-gutters">
            <?php
            // vars
            $hImage = get_field('h_image');
            $hColor = get_field('h_bg_color');
            $hTitle = get_field('h_title');
            $opened = get_field('opened');
            $arrow = get_field('arrow_color');
            ?>
            <div class="col-12 col-md-6 order-2 order-md-1">
                <div class="position-relative ">
                    <?php if (!empty($hImage)) { ?>
                        <img src="<?php echo $hImage; ?>" alt="" class="page-header__image">
                    <?php } ?>
                </div>
            </div>
            <div class="col-12 col-md-6 position-relative mobile-height order-1 order-md-2"
                 style="<?php if (!empty($hColor)) echo 'background-color:' . $hColor . '' ?>">
                <?php
                if (!empty($hTitle['text'])) { ?>
                    <h1 class="section-title" style="color: <?php echo $hTitle['color']; ?>">
                        <?php echo $hTitle['text']; ?>
                    </h1>
                <?php }
                if (!empty($opened)) { ?>
                    <div class="page-header__opened">
                        <div><strong>DESCHIS</strong><br/><?php echo $opened; ?></div>
                    </div>
                <?php } ?>
                <a href="#content" class="scroll-down">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25.199" height="28.799" viewBox="0 0 25.199 28.799">
                        <g id="arrow-down" transform="translate(-5.401 -3.6)">
                            <path id="Path_17" data-name="Path 17"
                                  d="M30.073,18.527a1.8,1.8,0,0,1,0,2.545l-10.8,10.8a1.8,1.8,0,0,1-2.545,0l-10.8-10.8a1.8,1.8,0,1,1,2.545-2.545L16.2,26.255V5.4a1.8,1.8,0,0,1,3.6,0V26.255l7.727-7.727a1.8,1.8,0,0,1,2.545,0Z"
                                  fill="<?php echo $arrow; ?>" fill-rule="evenodd"/>
                        </g>
                    </svg>
                </a>
            </div>
        </div>
    </div>
    <?php
    //vars
    $hours = get_field('opening_hours');
    $ialoc = get_field('ialoc');
    $site = get_field('site');
    $mail = get_field('mail');
    $phone = get_field('phone');
    $address = get_field('address');
    $map = get_field('map');
    $fb = get_field('facebook_link');
    $insta = get_field('instagram_link');
    $pinterest = get_field('pinterest_link');
    $yt = get_field('youtube_link');
    ?>
    <div class="container py-5 mt-md-4" id="content">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                    the_content();
                    // End the loop.
                endwhile;


                if (wp_is_mobile() && !empty($map)) { ?>
                <?php } ?>
                <a href="#" class="btn btn-outline-dark open-map d-block d-md-none">VIEW ON MAP</a>
                <hr class="my-5">
            </div>
            <div class="col-12 col-md-10">
                <div class="row">
                    <?php if (!empty(get_field('hours_title'))) { ?>
                        <div class="col-12 col-md-5 hours">

                            <h2 class="details-heading mb-4"><?php echo get_field('hours_title'); ?></h2>
                            <?php if (!empty($hours['monday'])) { ?>
                                <div class="row">
                                    <div class="col-5 col-md-4 hours__label">Monday</div>
                                    <div class="col-7 col-md-8 pr-md-0"><?php echo $hours['monday']; ?></div>
                                </div>
                            <?php }
                            if (!empty($hours['tuesday'])) { ?>
                                <div class="row">
                                    <div class="col-5 col-md-4 hours__label">Tuesday</div>
                                    <div class="col-7 col-md-8 pr-md-0"><?php echo $hours['tuesday']; ?></div>
                                </div>
                            <?php }
                            if (!empty($hours['wednesday'])) { ?>
                                <div class="row">
                                    <div class="col-5 col-md-4 hours__label">Wednesday</div>
                                    <div class="col-7 col-md-8 pr-md-0"><?php echo $hours['wednesday']; ?></div>
                                </div>
                            <?php }
                            if (!empty($hours['thursday'])) { ?>
                                <div class="row">
                                    <div class="col-5 col-md-4 hours__label">Thursday</div>
                                    <div class="col-7 col-md-8 pr-md-0"><?php echo $hours['thursday']; ?></div>
                                </div>
                            <?php }
                            if (!empty($hours['friday'])) { ?>
                                <div class="row">
                                    <div class="col-5 col-md-4 hours__label">Friday</div>
                                    <div class="col-7 col-md-8 pr-md-0"><?php echo $hours['friday']; ?></div>
                                </div>
                            <?php }
                            if (!empty($hours['saturday'])) { ?>
                                <div class="row">
                                    <div class="col-5 col-md-4 hours__label">Saturday
                                    </div>
                                    <div class="col-7 col-md-8 pr-md-0"><?php echo $hours['saturday']; ?></div>
                                </div>
                            <?php }
                            if (!empty($hours['sunday'])) { ?>
                                <div class="row">
                                    <div class="col-5 col-md-4 hours__label">Sunday</div>
                                    <div class="col-7 col-md-8 pr-md-0"><?php echo $hours['sunday']; ?></div>
                                </div>
                            <?php } ?>
                            <hr class="d-md-none">
                        </div>
                    <?php }

                    if (!empty(get_field('c_title'))) { ?>
                        <div class="col-6 col-md-4">
                            <h2 class="details-heading mb-4"><?php echo get_field('c_title'); ?></h2>
                            <?php if (!empty($site['title'])) { ?>
                                <a href="<?php echo $site['url']; ?>" target="_blank"
                                   rel="nofollow"><?php echo $site['title']; ?></a>
                            <?php }

                            if (!empty($mail)) { ?>
                                <a href="mailto:<?php echo $mail; ?>"><?php echo $mail; ?></a>
                            <?php }
                            if (!empty($phone)) { ?>
                                <div><?php echo $phone; ?></div>
                            <?php }
                            if (!empty($address)) { ?>
                                <div><?php echo $address; ?></div>
                            <?php } ?>
                        </div>
                    <?php }

                    if (!empty(get_field('s_title'))) { ?>
                        <div class="col-12 col-md-3 pr-md-0">
                            <hr class="d-md-none">
                            <h2 class="details-heading mb-4"><?php echo get_field('s_title'); ?></h2>
                            <?php if (!empty($insta)) { ?>
                                <div><i class="fab fa-instagram"></i> <a href="<?php echo $insta ?>">Instagram</a></div>
                            <?php }
                            if (!empty($fb)) { ?>
                                <div><i class="fab fa-facebook-f"></i> <a href="<?php echo $fb ?>">Facebook</a></div>
                            <?php }
                            if (!empty($pinterest)) { ?>
                                <div><i class="fab fa-pinterest-p"></i> <a href="<?php echo $pinterest ?>">Pinterest</a>
                                </div>
                            <?php }
                            if (!empty($yt)) { ?>
                                <div><i class="fab fa-youtube"></i> <a href="<?php echo $yt ?>">Youtube</a></div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
                <hr class="my-5">
            </div>
            <div class="col-12 col-md-10">
                <?php $terms = get_the_terms(get_the_ID(), 'places');

                $pin = get_field('pin', $terms[0]);

                if (!wp_is_mobile() && !empty($map)) { ?>
                    <div class="pb-5">
                        <div id="map"></div>
                    </div>
                <?php } ?>

                <div class="social-share">
                    <i class="fas fa-share-alt"></i> SHARE: <?php echo do_shortcode('[addtoany]'); ?>
                </div>
                <hr class="d-block d-md-none">
            </div>
        </div>
    </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBuDkks5RkO1J3xv51q5iKjxI7kycUsFHY&callback=initMap&libraries=&v=weekly"
        defer
></script>
<script>
    let map;
    const myLatLng = {lat: parseFloat(`<?php echo $map['lat']; ?>`), lng: parseFloat(`<?php echo $map['lng']; ?>`)};

    function initMap() {
        map = new google.maps.Map(document.getElementById("map"), {
            center: myLatLng,
            zoom: 15,
        });

        // Create popup
        const infowindow = new google.maps.InfoWindow({
            content: `<?php echo the_title(); ?><br/><?php echo $phone; ?><br/><?php echo $address; ?>`,
        });

        // Create marker.
        const marker = new google.maps.Marker({
            position: myLatLng,
            map,
            icon: `<?php echo $pin; ?>`,
        });

        marker.addListener("click", () => {
            infowindow.open(map, marker);
        });
    }
</script>
<?php get_footer(); ?>
